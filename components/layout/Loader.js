import style from './loader.module.scss'

export default function Loader() {
	return (
		<div className={style._body}></div>
	)
}