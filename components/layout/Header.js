import Burger from './Burger'
import Search from './Search'
import Link from 'next/link'
import style from './header.module.scss'
import { useRouter } from 'next/router'
import { useTranslation } from 'next-i18next'

export default function Header({ data }) {
	const { t } = useTranslation('common')
	const router = useRouter()
	const { categories } = data

	const { asPath } = router
	const categoryUrl = router.query.category

	return (
		<header className={style._wrapper}>
			<div className="container">
				<div className={style._line}></div>
				<div className={style._top}>
					<div className="row justify-content-between">
						<div className="col-8 col-md-3 mr-auto">
							<Link href="/">
								<a><img src="/icons/logo.svg" alt="Mainfin" title="Mainfin" /></a>
							</Link>
						</div>

						<div className="col-9 col-md-5 order-md-2 order-4 mt-4 mt-md-0">
							<Search />
						</div>

						<div className="col-4 col-md-2 order-2 order-md-3">
							<div className={style._languages}>
								{router.locales.map((locale, index) => (
									<Link href='/' locale={locale} key={index}>
										<a>{locale}</a>
									</Link>
								))}
							</div>
						</div>
						<div className="col-3 d-md-none order-3 mt-4 d-flex align-items-center">
							<Burger />
						</div>
					</div>
				</div>

				<nav className={`${style._navigation} d-none d-md-block`}>
					<ul className="row justify-content-between text-center">
						<li className="w-auto">
							<Link href="/">
								<a className={asPath == "/" ? `${style._link} ${style._link_current}` : `${style._link}`} >
									{t("homeLabel")}
								</a>
							</Link>
						</li>

						{categories.map((category, index) => (
							<li className="w-auto" key={index}>
								<Link href={`/${category.url}`}>
									<a className={categoryUrl == category.url ? `${style._link} ${style._link_current}` : `${style._link}`}>
										{category.title}
									</a>
								</Link>
							</li>
						))}
					</ul>
				</nav>

				<nav className={`${style._navigation} collapse`} id="collapseExample">
					<ul className="row flex-column justify-content-between text-center">
						<li className="w-auto">
							<Link href="/">
								<a className={asPath == "/" ? `${style._link} ${style._link_current}` : `${style._link}`} >
									{t("homeLabel")}
								</a>
							</Link>
						</li>

						{categories.map((category, index) => (
							<li className="w-auto" key={index}>
								<Link href={`/${category.url}`}>
									<a className={categoryUrl == category.url ? `${style._link} ${style._link_current}` : `${style._link}`}>
										{category.title}
									</a>
								</Link>
							</li>
						))}
					</ul>
				</nav>
			</div>
		</header >
	)
}