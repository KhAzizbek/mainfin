import style from './search.module.scss'
import { useTranslation } from 'next-i18next'

export default function Search() {
	const { t } = useTranslation('common')

	return (
		<div className={style._search}>
			<input type="text" name="search" placeholder={t("searchLabel")} />
			<button type="button">
				<img src="/icons/search.svg" alt={t("searchLabel")} />
			</button>
		</div>
	)
}

