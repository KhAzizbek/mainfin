import style from './categoryNewsMain.module.scss'
import Link from 'next/link'
import Image from 'next/image'

export default function CategoryNewsMain({ data }) {
	const { id, title, short_content, image, publication_date, url, category_url } = data
	let day, month, year

	const date = new Date(publication_date * 1000)
	day = date.getDate().toString().padStart(2, '0')
	month = (date.getMonth() + 1).toString().padStart(2, '0')
	year = date.getFullYear().toString().padStart(2, '0')

	const formattedDate = `${day}/${month}/${year}`

	return (
		<Link href={"/[category]/[article]"} as={`/${category_url}/${url}`}>
			<a className={style._body}>
				<div className="row">
					<div className="col-lg-8">
						<div className={style._photo}>
							<img src={image} title={title} alt={title} />
						</div>
					</div>

					<div className="col-lg-4">
						<div className={style._content}>
							<div className={style._date}>
								<img src="/icons/clock.svg" style={{ width: 12, height: 12, marginRight: 10 }} />
								{formattedDate}
							</div>
							<div>
								<h3 className={style._title}>
									{title}
								</h3>
								<p className={style._description}>
									{short_content}
								</p>
							</div>
						</div>
					</div>
				</div>
			</a>
		</Link>
	)
}