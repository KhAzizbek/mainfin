import style from './categoryNewsItem.module.scss'
import Link from 'next/link'
import Image from 'next/image'

export default function categoryNewsItem({ data }) {
	const { id, title, views, publication_date, short_content, image, url, category_url } = data
	let day, month, year

	const date = new Date(publication_date * 1000)
	day = date.getDate().toString().padStart(2, '0')
	month = (date.getMonth() + 1).toString().padStart(2, '0')
	year = date.getFullYear().toString().padStart(2, '0')

	const formattedDate = `${day}.${month}.${year}`

	return (
		<Link href={"/[category]/[article]"} as={`/${category_url}/${url}`}>
			<a className={style._body}>
				<div className={style._photo}>
					<img src={image} title={title} alt={title} />
				</div>
				<div className={style._content}>
					<div>
						<h3 className={style._title}>
							{title}
						</h3>
						<p className={style._description}>
							{short_content}
						</p>
					</div>
					<div className={style._statistics}>
						<div>{formattedDate}</div>
						<div>
							<img src="/icons/eye.svg" style={{ width: 15, height: 12, marginRight: 12 }} />
							{views}
						</div>
					</div>
				</div>
			</a>
		</Link>
	)
}