import Link from 'next/link'
import Image from 'next/image'
import style from './popularNewsItem.module.scss'

export default function popularNewsItem({ data }) {
	const { id, title, views, publication_date, image, url, category_url } = data
	let day, month, year

	const date = new Date(publication_date * 1000)
	day = date.getDate().toString().padStart(2, '0')
	month = (date.getMonth() + 1).toString().padStart(2, '0')
	year = date.getFullYear().toString().slice(-2).padStart(2, '0')

	const formattedDate = `${day}/${month}/${year}`

	return (
		<div className={style._item}>
			<div className={style._statistics}>
				<div>{formattedDate}</div>
				<div>
					<img src="/icons/eye.svg" style={{ width: 15, height: 12, marginRight: 10 }} />
					{views}
				</div>
			</div>
			<div className={style._information}>
				<Link href={"/[category]/[article]"} as={`/${category_url}/${url}`}>
					<a>
						<div className={style._photo}>
							<img src={image} title={title} alt={title} />
						</div>
						<div className={style._title}>{title}</div>
					</a>
				</Link>
			</div>
		</div>
	)
}