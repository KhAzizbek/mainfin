import Link from 'next/link'
import style from './footer.module.scss'
import { useTranslation } from 'next-i18next'

export default function Footer({ data }) {
	const { socials, categories, pages } = data
	const { t } = useTranslation('common')
	const date = new Date()
	const year = date.getFullYear()

	return (
		<footer className={style._wrapper}>
			<div className="container">
				<div className={style._line}></div>
				<div className={style._body}>
					<div className="row justify-content-center justify-content-md-around">
						<div className="col-sm-5 col-md-3 col-lg-2 order-1 mb-4">
							<p className={`${style._title} text-center text-sm-start`}>
								{t("socialsLabel")}
							</p>
							<div className={style._links}>
								{socials.map((social) => (
									<a href={social.url} target="_blank" key={social.id}>
										<img src={`/icons/${social.title.toLowerCase()}.svg`} alt={social.title} />
										{social.title}
									</a>
								))}
							</div>
						</div>

						<div className="col-sm-10 col-md-4 order-3 order-md-2 mb-4">
							<p className={`${style._title} text-center text-sm-start`}>
								{t("linksLabel")}
							</p>
							<div className={style._links}>
								{categories.map((category) => (
									<Link href={`/${category.url}`} key={category.id}>
										<a>{category.title}</a>
									</Link>
								))}
							</div>
						</div>

						<div className="col-sm-5  col-md-3 col-lg-2 order-2 order-md-3 mb-4">
							<p className={`${style._title} text-center text-sm-start`}>
								{t("aboutUsLabel")}
							</p>
							<div className={style._links}>
								{pages.map((page) => (
									<Link href={page.url} key={page.id}>
										<a>{page.title}</a>
									</Link>
								))}
							</div>
						</div>
					</div>
					<div className="row">
						<div className={`${style._copyright} col-12 `}>
							© <span id="copyrightYear">{year}</span>, {t("rightsLabel")} MainFin
						</div>
					</div>
				</div>
			</div>
		</footer>
	)
}
