import Link from 'next/link'
import style from './mainNews.module.scss'

export default function MainNews({ data }) {
	const { title, image, url, category_url } = data

	return (
		<Link href={"/[category]/[article]"} as={`/${category_url}/${url}`}>
			<a className={style._body}>
				<div className={style._photo}>
					<img src={image} title={title} alt={title} />
				</div>
				<div className={style._information}>
					<h4 className={style._title}>
						{title}
					</h4>
				</div>
			</a>
		</Link>
	)
}