import Link from 'next/link'
import style from './categorySection.module.scss'
import CategorySectionNewsMain from './CategorySectionNewsMain'
import CategorySectionNewsItem from './CategorySectionNewsItem'

export default function СategorySection({ category }) {
	const { articles, url, title, mainArticle } = category

	return (
		<section className={style._body}>
			<Link href={"/[category]"} as={`/${url}`}>
				<a>
					<h2 className={style._title}>{title}</h2>
				</a>
			</Link>

			<div className="row align-items-stretch">
				<div className="col-lg-7 mb-5 mb-lg-0">
					<CategorySectionNewsMain data={mainArticle} />
				</div>

				{articles && (
					<div className="col-lg-5">
						<div className="row h-100 align-items-start">
							{articles.map(item => (
								<div className="col-md-6 col-lg-12" key={item.id} style={{ marginBottom: 20 }}>
									<CategorySectionNewsItem data={item} />
								</div>
							))}
						</div>
					</div>
				)}
			</div>
		</section>
	)
}
