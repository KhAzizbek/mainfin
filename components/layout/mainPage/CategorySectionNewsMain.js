import style from './categorySectionNewsMain.module.scss'
import Link from 'next/link'
import Image from 'next/image'

export default function CategorySectionNewsMain({ data }) {
	const { id, title, image, url, publication_date, category_url } = data
	let day, month, year

	const date = new Date(publication_date * 1000)
	day = date.getDate().toString().padStart(2, '0')
	month = (date.getMonth() + 1).toString().padStart(2, '0')
	year = date.getFullYear().toString().padStart(2, '0')

	const formattedDate = `${day}/${month}/${year}`

	return (
		<Link href={"/[category]/[article]"} as={`/${category_url}/${url}`}>
			<a className={style._body}>
				<div className={style._photo}>
					<img src={image} title={title} alt={title} />
				</div>

				<div className={style._content}>
					<h3 className={style._title}>
						{title}
					</h3>
					<div className={style._date}>
						<img src="/icons/clock.svg" />
						{formattedDate}
					</div>
				</div>
			</a>
		</Link>
	)
}