import Link from 'next/link'
import style from './mainLatestNewsItem.module.scss'

export default function MainLatestNewsItem({ data }) {
	const { title, publication_date, image, url, category_url } = data
	let day, month, year

	const date = new Date(publication_date * 1000)
	day = date.getDate().toString().padStart(2, '0')
	month = (date.getMonth() + 1).toString().padStart(2, '0')
	year = date.getFullYear().toString().padStart(2, '0')

	const formattedDate = `${day}/${month}/${year}`

	return (
		<Link href={"/[category]/[article]"} as={`/${category_url}/${url}`}>
			<a className={style._body}>
				<div className={style._photo}>
					<img src={image} title={title} alt={title} />
				</div>

				<div className={style._content}>
					<div className={style._information}>
						<div>
							{formattedDate}
						</div>
						<div>
							<h3 className={style._category}>
								{category_url}
							</h3>
						</div>
					</div>

					<h4 className={style._title}>
						{title}
					</h4>
				</div>
			</a>
		</Link>
	)
}