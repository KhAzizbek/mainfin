import { signIn, signOut } from 'next-auth/client'

export default function Logout() {
    return (
        <>
            <p onClick={signIn}>Войти в кабинет</p>
            <p onClick={signOut}>Выйти из кабинет</p>
        </>
    )
}