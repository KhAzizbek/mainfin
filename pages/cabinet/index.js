import { signIn, getSession } from 'next-auth/client'

export default function Index({ data }) {

	return (
		<p onClick={signIn}>Войти в кабинет</p>
	)
}

export async function getServerSideProps(context) {
	const session = await getSession(context)

	if (session) {
		return {
			redirect: {
				destination: '/cabinet/my',
				permanent: true,
			},
		}
	}

	return {
		redirect: {
			destination: '/api/auth/signin',
			permanent: false,
		},
	}
}
